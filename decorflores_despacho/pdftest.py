import os
import textract

from pathlib import Path

def extract_information(pdf_path):
    with open(pdf_path, 'rb') as f:
        content = textract.process(pdf_path, method='pdfminer')

    return content


if __name__ == '__main__':
    filepath = str(Path('fac1.pdf').resolve())
    print(filepath)
    print(extract_information(filepath))
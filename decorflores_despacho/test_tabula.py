import os

from pathlib import Path
from tabula import read_pdf

def extract_information(pdf_path):
    df = read_pdf(pdf_path)
    return df

if __name__ == '__main__':
    filepath = str(Path('fac1.pdf').resolve())
    print(extract_information(filepath))